"use strict";

var bleno = require("@abandonware/bleno");

var SystemInformationService = require("./systeminformationservice");

var WifiService = require("./services/wifi-service");

var systemInformationService = new SystemInformationService();

const uidService = require("./services/uid-service");

 
uidService.hasGeneratedUid(hasUid => {
	if(hasUid){
  
		WifiService.checkInternet((IsConnected) => {
  
			//  FOR TESTING
			// IsConnected = false;
			// 
  
			if(IsConnected){
				console.log("Already Connected");
			}else{
				startBleAdvertisement();
			}
		}); 
  
	}else{
		console.log("No UID file found...");
		console.log("Please run `npm run init` first");
	}
});

 
function startBleAdvertisement(){


	bleno.on("stateChange", function(state) {
		console.log("on -> stateChange: " + state);
      
		if (state === "poweredOn") {
			var bname = bleno.name || "Reflect";
			bleno.startAdvertising(bname, [systemInformationService.uuid]);
		}else {
			bleno.stopAdvertising();
		}
	});
    
    
	bleno.on("advertisingStart", function(error) {
    
		console.log("on -> advertisingStart: " +
          (error ? "error " + error : "success")
		);
      
		if (!error) {
      
			bleno.setServices([
				systemInformationService
			]);
    
          
		}
	});

}

  
