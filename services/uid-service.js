"use strict";

const { v4: uuidv4 } = require("uuid");
var fs = require("fs");
const uidFilePath = "./uid/uids.json";
const QRCode = require("qrcode");


module.exports = {
	hasGeneratedUid(cb) {
		fs.access(uidFilePath, fs.F_OK, (err) => {
			if (err) {
				console.log("err",err);
				cb(false);
			}else{ 
				cb(true);
			}
		});
	},
	generateUid(){
		return {
			"primary": uuidv4(),
			"wifi": uuidv4(),
			"home": uuidv4()
		};
	},
	createUidFile(uids){
		return new Promise((resolve, reject) => {
			fs.writeFile(uidFilePath, JSON.stringify(uids), err => {
				if (err) {
					reject(err);
				}else{
					resolve("UID file created");
				}
			});
		});
	},

	createQrCodeFile(primaryUid){
		const opts = { 
			type: "png",
			quality: 1,
			margin: 1,
			scale: 30,
			color: {
				dark:"#21130d",
				light:"#eeeee4"
			}
		};
        
		return new Promise((resolve, reject) => {
			QRCode.toFile(
				"ble.png",
				[{ data: primaryUid.toUpperCase(), mode: "Alphanumeric" }],
				opts,
				function (err) {
					if (err){
						reject(err);
					}else{
						resolve("QR Code generated");
					}
				}
			);
		});  
       
	},
	getUids(){ 
	
		fs.readFile(uidFilePath, (err, rawdata) => {

			return new Promise((resolve, reject) => {
				if (err){
					reject(err);
				}else{  
					resolve(JSON.parse(rawdata));
				}            
			});
		});


	}
};