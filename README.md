<div align="center" id="top"> 
  <img src="./.github/app.gif" alt="Reflect Wifi Config" />

&#xa0;

  <!-- <a href="https://reflectwificonfig.netlify.app">Demo</a> -->
</div>

<h1 align="center">Reflect Wifi Config</h1>

<!-- Status -->

<h4 align="center">
	🚧  Reflect Wifi Config 🚀 Under construction...  🚧
</h4>

<hr>

<p align="center">
  <a href="#white_check_mark-requirements">Requirements</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-starting">Starting</a> &#xa0; | &#xa0;
</p>

<br>

## :white_check_mark: Requirements

Before starting :checkered_flag:

### System V

```bash
  $ sudo service bluetooth stop (once)
  $ sudo update-rc.d bluetooth remove (persist on reboot)
```

### systemd

```bash
  $ sudo systemctl stop bluetooth (once)
  $ sudo systemctl disable bluetooth (persist on reboot)
```

```bash
  $ sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

### Running on Linux

#### Running without root/sudo

Run the following command:

```sh
  $ sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

**Note:** The above command requires `setcap` to be installed, it can be installed using the following:

This grants the `node` binary `cap_net_raw` privileges, so it can start/stop BLE advertising.

```bash
  $ sudo apt-get install libcap2-bin
```

## :checkered_flag: Starting

```bash
# Clone this project
$ git clone git@gitlab.com:blocksoft1/reflect-wifi-connect.git

# Access
$ cd reflect-wifi-config

# Install dependencies
$ npm install

# Run the project
$ npm start

```

<a href="#top">Back to top</a>
