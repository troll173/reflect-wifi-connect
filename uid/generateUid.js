"use strict";

const uidService = require("../services/uid-service");
 
const uids = uidService.generateUid();

uidService.createUidFile(uids)
	.then(msg => {
		console.log(msg);
    
		uidService.createQrCodeFile(uids.primary)
			.then(res => {
				console.log(res);
				console.log("You can now use `npm start`");
			})
			.catch(err => {
				console.error(err);
			});

	})
	.catch(err => {
		console.error(err);
	});

