"use strict";

var bleno = require("@abandonware/bleno");

var util = require("util");
var Wifi = require("rpi-wifi-connection");

var wifi = new Wifi();

var WifiService = require("../services/wifi-service");

var BlenoCharacteristic = bleno.Characteristic;

var WifiCharacteristic = function() {
	WifiCharacteristic.super_.call(this, {
		uuid: "ff51b30e-d7e2-4d93-8842-a7c4a57dfb1b",
		properties: ["read", "write"],
	});

	this._value = Buffer.from("0");
	this.connected = false;
	this._readCallback = null;
}; 
 

WifiCharacteristic.prototype.onReadRequest = function(offset, callback) {

	WifiService.checkInternet((IsConnected) => {
  
		if(IsConnected){
			let bufferOne = Buffer.from("1");
			callback(this.RESULT_SUCCESS,  bufferOne);
			console.log("Connected to network.");
		}else{
			let bufferOne = Buffer.from("0");
			callback(this.RESULT_SUCCESS,  bufferOne);
			console.log("Not connected to network.");
		}
 
	}); 
 
};

WifiCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
	console.log("Write Request: ", data, offset, withoutResponse, callback);


	console.log("Received: |" + data.toString() + "|");


	var wifiSettings = data.toString().split(":");

	console.log("wifiSettings", wifiSettings);
  

	wifi.connect({ssid: wifiSettings[0], psk: wifiSettings[1]}).then(() => {
		console.log("Connected to network.");
		callback(this.RESULT_SUCCESS,  Buffer.from(JSON.stringify({})));
	})
		.catch((error) => {
			console.log("Connection Error.", error);
			callback(this.RESULT_ERROR, Buffer.from(JSON.stringify({})));
			return;
		});
};

 
    
util.inherits(WifiCharacteristic, BlenoCharacteristic);
module.exports = WifiCharacteristic;
