"use strict";

var bleno = require("@abandonware/bleno");


var util = require("util"); 
var fs = require("fs");
var exec = require("child_process").exec;

var BlenoCharacteristic = bleno.Characteristic;

var HomeCharacteristic = function() {
	HomeCharacteristic.super_.call(this, {
		uuid: "48d51a79-4ee8-41b9-8994-f58a78f6e7d6",
		properties: ["read", "write"],
	});

	this._value = new Buffer.from("0");
	this.filePath = "/home/pi/.reflect";
};


HomeCharacteristic.prototype.onReadRequest = function(offset, callback) {

	fs.access(this.filePath, fs.F_OK, (err) => {
		if (err) {
			console.error(err);
			let bufferOne = Buffer.from("0");
			callback(this.RESULT_SUCCESS,  bufferOne);
		}else{

        
			console.log("File exists");
			let bufferOne = Buffer.from("1");
			callback(this.RESULT_SUCCESS,  bufferOne);

			// bleno.stopAdvertising(res => {
			//     console.log("Advertisement stopped!", res);
			// })

			exec("reboot 0", function(error){
				console.log(error);
			});
		}
 
	});

};


HomeCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

	console.log("Received:  " + data.toString());
 
   
	var config = JSON.stringify({home: data.toString()});

	console.log("Writing:  " + config);


	fs.writeFile(this.filePath, config, function (err) {
		if (err) return console.log(err);
      
		console.log("written:  " + config);

		let bufferOne = Buffer.from("1");
		callback(this.RESULT_SUCCESS,  bufferOne);
	});

  

};

util.inherits(HomeCharacteristic, BlenoCharacteristic);
module.exports = HomeCharacteristic;
