"use strict";

var bleno = require("@abandonware/bleno");
var util = require("util");


var WifiCharacteristic = require("./characteristics/wifi");
var HomeCharacteristic = require("./characteristics/home");

function SystemInformationService() {

	bleno.PrimaryService.call(this, {
		uuid: "ff51b30e-d7e2-4d93-8842-a7c4a57dfb07",
		characteristics: [
			new WifiCharacteristic(),
			new HomeCharacteristic()
		]
	});
}

util.inherits(SystemInformationService, bleno.PrimaryService);
module.exports = SystemInformationService;
